package com.example.demo.Model;

public class Invoices {
    private int id ;
    private Customer customer ;
    private double ammount ;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public Customer getCustomer() {
        return customer;
    }


    public void setCustomer(Customer customer) {
        this.customer = customer;
    }


    public double getAmmount() {
        return ammount;
    }


    public void setAmmount(double ammount) {
        this.ammount = ammount;
    }


    public Invoices(int id, Customer customer, double ammount) {
        this.id = id;
        this.customer = customer;
        this.ammount = ammount;
    }


    public int getCustomerDiscount(){

        return 0 ;
    }
    
    public String getCustomerName(){

        return null ;
    }

    public double getAmountAfterDiscount(){
        return ammount - (ammount * customer.getDiscount()/100);
    }


    @Override
    public String toString() {
        return "Invoice [ id=" + id + ", customer=" + customer + ", amount=" + ammount + ",]";
    }
    
}

