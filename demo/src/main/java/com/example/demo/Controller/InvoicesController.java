package com.example.demo.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Invoices;
import com.example.demo.Service.InvoiceService;

@RestController
public class InvoicesController {
    
    @Autowired
    private InvoiceService InvoiceService;
    @GetMapping("/invoices")
    public ArrayList<Invoices> getInvoice(){
        ArrayList<Invoices> invoiceList = InvoiceService.getInvoiceList();
        return invoiceList ;
    }

}
